require 'rails_helper'

RSpec.describe Api::V1::LinksController, type: :request do
  describe 'create' do
    let(:json) { JSON.parse(response.body) }
    let(:perx_link) { 'http://www.perxtech.com/' }
    let(:shorten_link) { 'short' }
    let(:service) do
      double('ShortenService',
             origin_link: perx_link,
             shorten_link: shorten_link,
             slice: { origin_link: perx_link,
                      shorten_link: shorten_link })
    end
    let!(:link_record) { create(:link, origin_link: 'http://origin.com', shorten_link: 'shorten') }

    context 'creates shorten link' do
      before do
        allow(ShortenLinkService).to receive(:call).and_return(service)
      end

      it 'responses success' do
        post '/api/v1/links/'
        expect(response).to be_success
      end

      it 'contains origin link' do
        post '/api/v1/links/', params: { link: perx_link }
        expect(json).to include('origin_link' => perx_link)
      end

      it 'contains shorten link' do
        post '/api/v1/links/', params: { link: perx_link }
        expect(json).to include('shorten_link' => shorten_link)
      end

      it 'saves shorten link into database' do
        post '/api/v1/links/', params: { link: perx_link }
        expect(Link.last.shorten_link).to eq shorten_link
      end

      it 'saves origin link into database' do
        post '/api/v1/links/', params: { link: perx_link }
        expect(Link.last.origin_link).to eq perx_link
      end

      it 'renders the Link from database' do
        allow(LinkCreatorService).to receive_message_chain('call.link').and_return(link_record)
        allow(LinkCreatorService).to receive_message_chain('call.valid?').and_return(true)
        post '/api/v1/links/', params: { link: perx_link }
        expect(json).to include(link_record.slice(:origin_link, :shorten_link))
      end
    end

    describe 'create link unsuccessfully' do
      it 'render errors for invalid link' do
        post '/api/v1/links/', params: { link: 'not valid' }
        expect(json).to include('origin_link' => ['is invalid'])
      end
    end
  end
end
