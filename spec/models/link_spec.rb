require 'rails_helper'

RSpec.describe Link, type: :model do
  it { should validate_presence_of(:origin_link) }
  it { should validate_presence_of(:shorten_link) }
  it { should validate_uniqueness_of(:origin_link) }
  it { should validate_uniqueness_of(:shorten_link) }
  it { should allow_value('https://perx.com').for(:origin_link) }
  it { should_not allow_value('bar.com').for(:origin_link) }
end
