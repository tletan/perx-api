require 'rails_helper'

RSpec.describe DispatcherController, type: :controller do

  describe 'show' do
    let(:perx_link) { 'http://www.perxtech.com/' }
    let(:shorten_link) { 'short' }
    let!(:link) { create(:link, origin_link: perx_link, shorten_link: shorten_link) }

    it 'redirects to origin link' do
      get :show, params: { id: shorten_link }
      expect(response).to have_http_status(301)
    end
  end
end
