require 'rails_helper'

RSpec.describe LinkCreatorService do
  let(:perx_link) { 'http://www.perxtech.com/' }
  let(:shorten_link) { 'perx.tech/short' }

  describe '#call' do
    it 'creates a link' do
      link = LinkCreatorService.call(origin_link: perx_link, shorten_link: shorten_link).link
      expect(link.origin_link).to eq perx_link
      expect(link.shorten_link).to eq shorten_link
    end

    it 'yields block when given' do
      called = false
      service = LinkCreatorService.call(origin_link: perx_link, shorten_link: shorten_link) do |s|
        called = true
      end

      expect(called).to be_truthy
    end

    it 'returns existed shorten link if existed' do
      a_link = create(:link, origin_link: perx_link, shorten_link: shorten_link)
      service = LinkCreatorService.call(origin_link: perx_link, shorten_link: shorten_link)
      expect(service.link.id).to eq a_link.id
    end
  end

  describe '#retry_with' do
    let!(:existed_link) { create(:link, origin_link: perx_link, shorten_link: shorten_link) }

    it 'creates link with new shorten link' do
      service = LinkCreatorService.call(origin_link: 'http://origin.com', shorten_link: shorten_link) do |s|
        s.retry_with('new shorten link')
      end

      expect(service.link.shorten_link).to eq 'new shorten link'
    end
  end
end
