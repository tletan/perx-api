require 'rails_helper'

RSpec.describe ShortenLinkService do
  describe '#call' do
    let(:perx_link) { 'http://www.perxtech.com/' }
    let(:service) { ShortenLinkService.call(link: perx_link) }
    let(:shuffle_chars) { %w[p e r x r o c k s 2 0 1 8] }

    it 'saves origin link' do
      expect(service.origin_link).to eq perx_link
    end

    it 'generates shorten link' do
      allow_any_instance_of(Array).to receive(:shuffle).and_return(shuffle_chars)
      expect(service.shorten_link).to be_kind_of(String)
      expect(service.shorten_link).to eq 'perxroc'
    end
  end
end
