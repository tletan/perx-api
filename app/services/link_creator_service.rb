require 'forwardable'

class LinkCreatorService
  extend Forwardable

  def_delegators :@result, :errors, :valid?

  def self.call(*params, &block)
    new(*params).call(&block)
  end

  attr_reader :origin_link, :shorten_link, :scope, :result

  def initialize(origin_link:, shorten_link:, scope: Link)
    @origin_link = origin_link
    @shorten_link = shorten_link
    @scope = scope
  end

  def link
    result
  end

  def shorten_link_valid?
    result.errors[:shorten_link].blank?
  end

  def call
    @result = create_record
    yield self if block_given?

    self
  end

  def retry_with(new_shorten_link)
    @shorten_link = new_shorten_link
    @result = create_record
  end

  private

  def create_record
    scope
      .create_with(shorten_link: shorten_link)
      .find_or_create_by(origin_link: origin_link)
  end
end
