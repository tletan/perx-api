# frozen_string_literal: true

class ShortenLinkService

  def self.call(*params)
    new(*params).call
  end

  attr_reader :link

  alias origin_link link

  def initialize(link:)
    @link = link
  end

  def call
    self
  end

  # dont need to use freeze, use frozen string literal instead
  CHARS = ['a'..'z', 0..9, 'A'..'Z']
  MAX_LENGTH = 7

  def shorten_link
    CHARS.map(&:to_a).flatten.shuffle.take(MAX_LENGTH).join
  end
end
