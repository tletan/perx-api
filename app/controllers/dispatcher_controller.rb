class DispatcherController < ApplicationController

  def show
    link = Link.find_by(shorten_link: params.fetch(:id))
    if link.present?
      link.with_lock do
        link.clicks += 1
        link.save
      end
      redirect_to link.origin_link, status: :moved_permanently
    end
  end
end
