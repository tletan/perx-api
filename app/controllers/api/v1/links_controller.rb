class Api::V1::LinksController < ApplicationController
  def create
    service = ShortenLinkService.call(link: params.delete(:link))
    link_creator = LinkCreatorService.call(origin_link: service.origin_link, shorten_link: service.shorten_link) do |s|
      s.retry_with(service.shorten_link) until s.shorten_link_valid?
    end

    if link_creator.valid?
      render json: link_creator.link, status: :created
    else
      render json: link_creator.errors.messages, status: :unprocessable_entity
    end
  end
end
