require 'uri'

class Link < ApplicationRecord
  validates :origin_link, :shorten_link, presence: true, uniqueness: true
  validates_format_of :origin_link, with: URI::regexp
end
