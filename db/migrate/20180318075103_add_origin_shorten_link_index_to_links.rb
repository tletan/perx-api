class AddOriginShortenLinkIndexToLinks < ActiveRecord::Migration[5.1]
  def change
    add_index :links, :origin_link, unique: true
    add_index :links, :shorten_link, unique: true
  end
end
