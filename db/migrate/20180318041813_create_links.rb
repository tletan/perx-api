class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :origin_link
      t.string :shorten_link

      t.timestamps
    end
  end
end
